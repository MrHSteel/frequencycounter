document.getElementById("countButton").onclick = function () {
    
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    countLetters(typedText);
    const words = typedText.split(" ");
}
function countLetters(theText) {
    let letterCount = {};

    for (let i = 0; i < theText.length; i++) {

        let currentLetter = theText[i];
        if (letterCount[currentLetter] === undefined) {
            letterCount[currentLetter] = 1;
        } else {
            letterCount[currentLetter]++;
        }
        
    }
   
    document.getElementById("lettersDiv");
    let divContent = ""
    for (let key in letterCount){
        divContent += "'"+key + "':" + letterCount[key] + " "
    }
    let text = document.createTextNode(divContent);
    lettersDiv.appendChild(text);
}


function countWord(words) {
    let wordCount = {};
    for (let i = 0; i < words.length; i++) {

        let currentWord = words[i];
        if (wordCount[currentWord] === undefined) {
            wordCount[currentWord] = 1;
        } else {
            wordCount[currentWord]++;
        }

    }
    document.getElementById("wordsDiv");
    let divContent = ""
    for (let key in wordCount){
        divContent += key + ":" + wordCount[key] + " "
    }
    let text = document.createTextNode(divContent);
    wordsDiv.appendChild(text);
}